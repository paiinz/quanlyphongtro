/*
 * Created by JFormDesigner on Mon May 18 09:01:00 ICT 2020
 */

package org.haui.view;

import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author Jame mark
 */
public class RegisterView extends JFrame {
    public RegisterView() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        lblUserName = new JLabel();
        lblPassword = new JLabel();
        txtUserName = new JTextField();
        txtPassword = new JTextField();
        lblRepeatPassword = new JLabel();
        txtRepeatPassword = new JTextField();
        lblAdminToken = new JLabel();
        txtAdminToken = new JTextField();
        btnRegister = new JButton();
        btnCancel = new JButton();

        //======== this ========
        var contentPane = getContentPane();

        //---- lblUserName ----
        lblUserName.setText("T\u00ean \u0111\u0103ng nh\u1eadp");

        //---- lblPassword ----
        lblPassword.setText("M\u1eadt kh\u1ea9u");

        //---- lblRepeatPassword ----
        lblRepeatPassword.setText("Nh\u1eadp l\u1ea1i MK");

        //---- lblAdminToken ----
        lblAdminToken.setText("Admin token");

        //---- btnRegister ----
        btnRegister.setText("\u0110\u0103ng k\u00fd");

        //---- btnCancel ----
        btnCancel.setText("H\u1ee7y");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(lblUserName)
                        .addComponent(lblRepeatPassword)
                        .addComponent(lblPassword)
                        .addComponent(lblAdminToken))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(btnRegister)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnCancel))
                        .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtUserName, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                            .addComponent(txtRepeatPassword, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                            .addComponent(txtPassword, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                            .addComponent(txtAdminToken, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)))
                    .addContainerGap(25, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(43, 43, 43)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(lblUserName)
                        .addComponent(txtUserName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(lblPassword)
                        .addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(13, 13, 13)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(lblRepeatPassword)
                        .addComponent(txtRepeatPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(lblAdminToken)
                        .addComponent(txtAdminToken, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(btnRegister)
                        .addComponent(btnCancel))
                    .addContainerGap(26, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel lblUserName;
    private JLabel lblPassword;
    private JTextField txtUserName;
    private JTextField txtPassword;
    private JLabel lblRepeatPassword;
    private JTextField txtRepeatPassword;
    private JLabel lblAdminToken;
    private JTextField txtAdminToken;
    private JButton btnRegister;
    private JButton btnCancel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    public JLabel getLblUserName() {
        return lblUserName;
    }

    public void setLblUserName(JLabel lblUserName) {
        this.lblUserName = lblUserName;
    }

    public JLabel getLblPassword() {
        return lblPassword;
    }

    public void setLblPassword(JLabel lblPassword) {
        this.lblPassword = lblPassword;
    }

    public JTextField getTxtUserName() {
        return txtUserName;
    }

    public void setTxtUserName(JTextField txtUserName) {
        this.txtUserName = txtUserName;
    }

    public JTextField getTxtPassword() {
        return txtPassword;
    }

    public void setTxtPassword(JTextField txtPassword) {
        this.txtPassword = txtPassword;
    }

    public JLabel getLblRepeatPassword() {
        return lblRepeatPassword;
    }

    public void setLblRepeatPassword(JLabel lblRepeatPassword) {
        this.lblRepeatPassword = lblRepeatPassword;
    }

    public JTextField getTxtRepeatPassword() {
        return txtRepeatPassword;
    }

    public void setTxtRepeatPassword(JTextField txtRepeatPassword) {
        this.txtRepeatPassword = txtRepeatPassword;
    }

    public JLabel getLblAdminToken() {
        return lblAdminToken;
    }

    public void setLblAdminToken(JLabel lblAdminToken) {
        this.lblAdminToken = lblAdminToken;
    }

    public JTextField getTxtAdminToken() {
        return txtAdminToken;
    }

    public void setTxtAdminToken(JTextField txtAdminToken) {
        this.txtAdminToken = txtAdminToken;
    }

    public JButton getBtnRegister() {
        return btnRegister;
    }

    public void setBtnRegister(JButton btnRegister) {
        this.btnRegister = btnRegister;
    }

    public JButton getBtnCancel() {
        return btnCancel;
    }

    public void setBtnCancel(JButton btnCancel) {
        this.btnCancel = btnCancel;
    }
}
