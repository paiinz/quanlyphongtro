/*
 * Created by JFormDesigner on Mon May 25 08:38:47 ICT 2020
 */

package org.haui.view;

import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author Jame mark
 */
public class RoomManagementView extends JFrame {
    public RoomManagementView() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        tabRoom = new JTabbedPane();
        panelRoom = new JPanel();
        lblRoomName = new JLabel();
        txtRoomName = new JTextField();
        lblRoomPrice = new JLabel();
        txtRoomPrice = new JTextField();
        lblRoomAddress = new JLabel();
        txtRoomAddress = new JTextField();
        lblRoomType = new JLabel();
        cbRoomType = new JComboBox();
        scrollPane3 = new JScrollPane();
        listRoom = new JList();
        btnAddRoom = new JButton();
        btnDeleteRoom = new JButton();
        btnUpdateRoom = new JButton();
        btnCancelRoom = new JButton();
        panelRoomType = new JPanel();
        lblRoomTypeName = new JLabel();
        txtRoomTypeName = new JTextField();
        txtRoomTypePerson = new JTextField();
        lblRoomTypePerson = new JLabel();
        lblRoomTypeDes = new JLabel();
        scrollPane1 = new JScrollPane();
        txtRoomTypeDes = new JTextArea();
        btnAddRoomType = new JButton();
        scrollPane2 = new JScrollPane();
        listRoomType = new JList();
        btnDeleteRoomType = new JButton();
        btnUpdateRoomType = new JButton();
        btnCancelRoomType = new JButton();

        //======== this ========
        var contentPane = getContentPane();

        //======== tabRoom ========
        {

            //======== panelRoom ========
            {

                //---- lblRoomName ----
                lblRoomName.setText("T\u00ean ph\u00f2ng");

                //---- lblRoomPrice ----
                lblRoomPrice.setText("Gi\u00e1");

                //---- lblRoomAddress ----
                lblRoomAddress.setText("\u0110\u1ecba ch\u1ec9");

                //---- lblRoomType ----
                lblRoomType.setText("Lo\u1ea1i ph\u00f2ng");

                //======== scrollPane3 ========
                {
                    scrollPane3.setViewportView(listRoom);
                }

                //---- btnAddRoom ----
                btnAddRoom.setText("Th\u00eam");

                //---- btnDeleteRoom ----
                btnDeleteRoom.setText("X\u00f3a");

                //---- btnUpdateRoom ----
                btnUpdateRoom.setText("C\u1eadp nh\u1eadt");

                //---- btnCancelRoom ----
                btnCancelRoom.setText("H\u1ee7y");

                GroupLayout panelRoomLayout = new GroupLayout(panelRoom);
                panelRoom.setLayout(panelRoomLayout);
                panelRoomLayout.setHorizontalGroup(
                    panelRoomLayout.createParallelGroup()
                        .addGroup(panelRoomLayout.createSequentialGroup()
                            .addGap(15, 15, 15)
                            .addGroup(panelRoomLayout.createParallelGroup()
                                .addComponent(scrollPane3, GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
                                .addGroup(panelRoomLayout.createSequentialGroup()
                                    .addGroup(panelRoomLayout.createParallelGroup()
                                        .addComponent(lblRoomName)
                                        .addComponent(lblRoomPrice)
                                        .addComponent(lblRoomAddress)
                                        .addComponent(lblRoomType))
                                    .addGap(29, 29, 29)
                                    .addGroup(panelRoomLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtRoomName, GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                                        .addComponent(txtRoomPrice, GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                                        .addComponent(txtRoomAddress, GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                                        .addComponent(cbRoomType, GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE))
                                    .addGap(39, 39, 39)
                                    .addGroup(panelRoomLayout.createParallelGroup()
                                        .addComponent(btnAddRoom)
                                        .addComponent(btnDeleteRoom)
                                        .addComponent(btnUpdateRoom)
                                        .addComponent(btnCancelRoom))
                                    .addGap(0, 353, Short.MAX_VALUE)))
                            .addContainerGap())
                );
                panelRoomLayout.setVerticalGroup(
                    panelRoomLayout.createParallelGroup()
                        .addGroup(panelRoomLayout.createSequentialGroup()
                            .addGap(18, 18, 18)
                            .addGroup(panelRoomLayout.createParallelGroup()
                                .addGroup(panelRoomLayout.createSequentialGroup()
                                    .addGroup(panelRoomLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblRoomName)
                                        .addComponent(txtRoomName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panelRoomLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblRoomPrice)
                                        .addComponent(txtRoomPrice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panelRoomLayout.createParallelGroup()
                                        .addComponent(lblRoomAddress)
                                        .addComponent(txtRoomAddress, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                    .addGap(7, 7, 7)
                                    .addGroup(panelRoomLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblRoomType)
                                        .addComponent(cbRoomType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                                .addGroup(panelRoomLayout.createSequentialGroup()
                                    .addComponent(btnAddRoom)
                                    .addGap(6, 6, 6)
                                    .addComponent(btnDeleteRoom)
                                    .addGap(6, 6, 6)
                                    .addComponent(btnUpdateRoom)
                                    .addGap(6, 6, 6)
                                    .addComponent(btnCancelRoom)))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(scrollPane3, GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                            .addContainerGap())
                );
            }
            tabRoom.addTab("Ph\u00f2ng", panelRoom);

            //======== panelRoomType ========
            {

                //---- lblRoomTypeName ----
                lblRoomTypeName.setText("T\u00ean lo\u1ea1i ph\u00f2ng");

                //---- lblRoomTypePerson ----
                lblRoomTypePerson.setText("S\u1ed1 ng\u01b0\u1eddi");

                //---- lblRoomTypeDes ----
                lblRoomTypeDes.setText("Mi\u00eau t\u1ea3");

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(txtRoomTypeDes);
                }

                //---- btnAddRoomType ----
                btnAddRoomType.setText("Th\u00eam");

                //======== scrollPane2 ========
                {
                    scrollPane2.setViewportView(listRoomType);
                }

                //---- btnDeleteRoomType ----
                btnDeleteRoomType.setText("X\u00f3a");

                //---- btnUpdateRoomType ----
                btnUpdateRoomType.setText("C\u1eadp nh\u1eadt");

                //---- btnCancelRoomType ----
                btnCancelRoomType.setText("H\u1ee7y");

                GroupLayout panelRoomTypeLayout = new GroupLayout(panelRoomType);
                panelRoomType.setLayout(panelRoomTypeLayout);
                panelRoomTypeLayout.setHorizontalGroup(
                    panelRoomTypeLayout.createParallelGroup()
                        .addGroup(panelRoomTypeLayout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panelRoomTypeLayout.createParallelGroup()
                                .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 789, Short.MAX_VALUE)
                                .addGroup(panelRoomTypeLayout.createSequentialGroup()
                                    .addGroup(panelRoomTypeLayout.createParallelGroup()
                                        .addComponent(lblRoomTypeName)
                                        .addComponent(lblRoomTypePerson)
                                        .addComponent(lblRoomTypeDes))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panelRoomTypeLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtRoomTypeName, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                                        .addComponent(txtRoomTypePerson, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE))
                                    .addGap(32, 32, 32)
                                    .addGroup(panelRoomTypeLayout.createParallelGroup()
                                        .addComponent(btnAddRoomType)
                                        .addComponent(btnDeleteRoomType)
                                        .addComponent(btnUpdateRoomType)
                                        .addComponent(btnCancelRoomType))
                                    .addGap(0, 304, Short.MAX_VALUE)))
                            .addContainerGap())
                );
                panelRoomTypeLayout.setVerticalGroup(
                    panelRoomTypeLayout.createParallelGroup()
                        .addGroup(panelRoomTypeLayout.createSequentialGroup()
                            .addGap(12, 12, 12)
                            .addGroup(panelRoomTypeLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(lblRoomTypeName)
                                .addComponent(txtRoomTypeName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnAddRoomType))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panelRoomTypeLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(lblRoomTypePerson)
                                .addComponent(txtRoomTypePerson, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnDeleteRoomType))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panelRoomTypeLayout.createParallelGroup()
                                .addComponent(lblRoomTypeDes)
                                .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
                                .addGroup(panelRoomTypeLayout.createSequentialGroup()
                                    .addComponent(btnUpdateRoomType)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnCancelRoomType)))
                            .addGap(11, 11, 11)
                            .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                            .addContainerGap())
                );
            }
            tabRoom.addTab("Lo\u1ea1i ph\u00f2ng", panelRoomType);
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(tabRoom)
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(tabRoom)
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JTabbedPane tabRoom;
    private JPanel panelRoom;
    private JLabel lblRoomName;
    private JTextField txtRoomName;
    private JLabel lblRoomPrice;
    private JTextField txtRoomPrice;
    private JLabel lblRoomAddress;
    private JTextField txtRoomAddress;
    private JLabel lblRoomType;
    private JComboBox cbRoomType;
    private JScrollPane scrollPane3;
    private JList listRoom;
    private JButton btnAddRoom;
    private JButton btnDeleteRoom;
    private JButton btnUpdateRoom;
    private JButton btnCancelRoom;
    private JPanel panelRoomType;
    private JLabel lblRoomTypeName;
    private JTextField txtRoomTypeName;
    private JTextField txtRoomTypePerson;
    private JLabel lblRoomTypePerson;
    private JLabel lblRoomTypeDes;
    private JScrollPane scrollPane1;
    private JTextArea txtRoomTypeDes;
    private JButton btnAddRoomType;
    private JScrollPane scrollPane2;
    private JList listRoomType;
    private JButton btnDeleteRoomType;
    private JButton btnUpdateRoomType;
    private JButton btnCancelRoomType;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    public JTabbedPane getTabRoom() {
        return tabRoom;
    }

    public void setTabRoom(JTabbedPane tabRoom) {
        this.tabRoom = tabRoom;
    }

    public JPanel getPanelRoom() {
        return panelRoom;
    }

    public void setPanelRoom(JPanel panelRoom) {
        this.panelRoom = panelRoom;
    }

    public JPanel getPanelRoomType() {
        return panelRoomType;
    }

    public void setPanelRoomType(JPanel panelRoomType) {
        this.panelRoomType = panelRoomType;
    }

    public JLabel getLblRoomTypeName() {
        return lblRoomTypeName;
    }

    public void setLblRoomTypeName(JLabel lblRoomTypeName) {
        this.lblRoomTypeName = lblRoomTypeName;
    }

    public JTextField getTxtRoomTypeName() {
        return txtRoomTypeName;
    }

    public void setTxtRoomTypeName(JTextField txtRoomTypeName) {
        this.txtRoomTypeName = txtRoomTypeName;
    }

    public JTextField getTxtRoomTypePerson() {
        return txtRoomTypePerson;
    }

    public void setTxtRoomTypePerson(JTextField txtRoomTypePerson) {
        this.txtRoomTypePerson = txtRoomTypePerson;
    }

    public JLabel getLblRoomTypePerson() {
        return lblRoomTypePerson;
    }

    public void setLblRoomTypePerson(JLabel lblRoomTypePerson) {
        this.lblRoomTypePerson = lblRoomTypePerson;
    }

    public JLabel getLblRoomTypeDes() {
        return lblRoomTypeDes;
    }

    public void setLblRoomTypeDes(JLabel lblRoomTypeDes) {
        this.lblRoomTypeDes = lblRoomTypeDes;
    }

    public JScrollPane getScrollPane1() {
        return scrollPane1;
    }

    public void setScrollPane1(JScrollPane scrollPane1) {
        this.scrollPane1 = scrollPane1;
    }

    public JTextArea getTxtRoomTypeDes() {
        return txtRoomTypeDes;
    }

    public void setTxtRoomTypeDes(JTextArea txtRoomTypeDes) {
        this.txtRoomTypeDes = txtRoomTypeDes;
    }

    public JButton getBtnAddRoomType() {
        return btnAddRoomType;
    }

    public void setBtnAddRoomType(JButton btnAddRoomType) {
        this.btnAddRoomType = btnAddRoomType;
    }

    public JScrollPane getScrollPane2() {
        return scrollPane2;
    }

    public void setScrollPane2(JScrollPane scrollPane2) {
        this.scrollPane2 = scrollPane2;
    }

    public JList getListRoomType() {
        return listRoomType;
    }

    public void setListRoomType(JList listRoomType) {
        this.listRoomType = listRoomType;
    }

    public JButton getBtnDeleteRoomType() {
        return btnDeleteRoomType;
    }

    public void setBtnDeleteRoomType(JButton btnDeleteRoomType) {
        this.btnDeleteRoomType = btnDeleteRoomType;
    }

    public JButton getBtnUpdateRoomType() {
        return btnUpdateRoomType;
    }

    public void setBtnUpdateRoomType(JButton btnUpdateRoomType) {
        this.btnUpdateRoomType = btnUpdateRoomType;
    }

    public JButton getBtnCancelRoomType() {
        return btnCancelRoomType;
    }

    public void setBtnCancelRoomType(JButton btnCancelRoomType) {
        this.btnCancelRoomType = btnCancelRoomType;
    }

    public JLabel getLblRoomName() {
        return lblRoomName;
    }

    public void setLblRoomName(JLabel lblRoomName) {
        this.lblRoomName = lblRoomName;
    }

    public JTextField getTxtRoomName() {
        return txtRoomName;
    }

    public void setTxtRoomName(JTextField txtRoomName) {
        this.txtRoomName = txtRoomName;
    }

    public JLabel getLblRoomPrice() {
        return lblRoomPrice;
    }

    public void setLblRoomPrice(JLabel lblRoomPrice) {
        this.lblRoomPrice = lblRoomPrice;
    }

    public JTextField getTxtRoomPrice() {
        return txtRoomPrice;
    }

    public void setTxtRoomPrice(JTextField txtRoomPrice) {
        this.txtRoomPrice = txtRoomPrice;
    }

    public JLabel getLblRoomAddress() {
        return lblRoomAddress;
    }

    public void setLblRoomAddress(JLabel lblRoomAddress) {
        this.lblRoomAddress = lblRoomAddress;
    }

    public JTextField getTxtRoomAddress() {
        return txtRoomAddress;
    }

    public void setTxtRoomAddress(JTextField txtRoomAddress) {
        this.txtRoomAddress = txtRoomAddress;
    }

    public JLabel getLblRoomType() {
        return lblRoomType;
    }

    public void setLblRoomType(JLabel lblRoomType) {
        this.lblRoomType = lblRoomType;
    }

    public JComboBox getCbRoomType() {
        return cbRoomType;
    }

    public void setCbRoomType(JComboBox cbRoomType) {
        this.cbRoomType = cbRoomType;
    }

    public JScrollPane getScrollPane3() {
        return scrollPane3;
    }

    public void setScrollPane3(JScrollPane scrollPane3) {
        this.scrollPane3 = scrollPane3;
    }

    public JList getListRoom() {
        return listRoom;
    }

    public void setListRoom(JList listRoom) {
        this.listRoom = listRoom;
    }

    public JButton getBtnAddRoom() {
        return btnAddRoom;
    }

    public void setBtnAddRoom(JButton btnAddRoom) {
        this.btnAddRoom = btnAddRoom;
    }

    public JButton getBtnDeleteRoom() {
        return btnDeleteRoom;
    }

    public void setBtnDeleteRoom(JButton btnDeleteRoom) {
        this.btnDeleteRoom = btnDeleteRoom;
    }

    public JButton getBtnUpdateRoom() {
        return btnUpdateRoom;
    }

    public void setBtnUpdateRoom(JButton btnUpdateRoom) {
        this.btnUpdateRoom = btnUpdateRoom;
    }

    public JButton getBtnCancelRoom() {
        return btnCancelRoom;
    }

    public void setBtnCancelRoom(JButton btnCancelRoom) {
        this.btnCancelRoom = btnCancelRoom;
    }
}
