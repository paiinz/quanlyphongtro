/*
 * Created by JFormDesigner on Sat May 16 09:30:29 ICT 2020
 */

package org.haui.view;

import java.awt.*;
import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author Jame mark
 */
public class MainView extends JFrame {
    public MainView() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        label1 = new JLabel();
        lblWelcomeBack = new JLabel();
        btnRoomManagement = new JButton();
        btnGuestManagement = new JButton();

        //======== this ========
        var contentPane = getContentPane();

        //---- label1 ----
        label1.setText("Main Application");

        //---- lblWelcomeBack ----
        lblWelcomeBack.setText("Welcome back : {{userName}}");

        //---- btnRoomManagement ----
        btnRoomManagement.setText("Qu\u1ea3n l\u00fd ph\u00f2ng tr\u1ecd");

        //---- btnGuestManagement ----
        btnGuestManagement.setText("Qu\u1ea3n l\u00fd kh\u00e1ch");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(label1)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblWelcomeBack, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnRoomManagement, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnGuestManagement, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(307, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label1)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(lblWelcomeBack)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(btnRoomManagement)
                        .addComponent(btnGuestManagement))
                    .addContainerGap(262, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel label1;
    private JLabel lblWelcomeBack;
    private JButton btnRoomManagement;
    private JButton btnGuestManagement;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    public JLabel getLabel1() {
        return label1;
    }

    public void setLabel1(JLabel label1) {
        this.label1 = label1;
    }

    public JLabel getLblWelcomeBack() {
        return lblWelcomeBack;
    }

    public void setLblWelcomeBack(JLabel lblWelcomeBack) {
        this.lblWelcomeBack = lblWelcomeBack;
    }

    public JButton getBtnRoomManagement() {
        return btnRoomManagement;
    }

    public void setBtnRoomManagement(JButton btnRoomManagement) {
        this.btnRoomManagement = btnRoomManagement;
    }

    public JButton getBtnGuestManagement() {
        return btnGuestManagement;
    }

    public void setBtnGuestManagement(JButton btnGuestManagement) {
        this.btnGuestManagement = btnGuestManagement;
    }
}
