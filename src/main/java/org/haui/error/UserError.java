package org.haui.error;

public class UserError extends Exception{
    public UserError(String message) {
        super(message);
    }
}
