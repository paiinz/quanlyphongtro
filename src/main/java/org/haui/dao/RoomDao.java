package org.haui.dao;

import org.haui.common.util.HibernateUtil;
import org.haui.model.Room;
import org.haui.model.RoomType;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class RoomDao implements DaoCommon<Room, Long>{

    @Override
    public Room create(Room room) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Long id = (Long) session.save(room);
        transaction.commit();
        session.close();
        room.setId(id);
        return room;
    }

    @Override
    public Room update(Room room) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(room);
        transaction.commit();
        session.close();
        return room;
    }

    @Override
    public void delete(Room room) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(room);
        transaction.commit();
        session.close();
    }

    @Override
    public void deleteById(Long aLong) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Room room = session.createQuery("select Room from Room where Room .id =: id", Room.class).setParameter("id", aLong).getSingleResult();
        session.delete(room);
        transaction.commit();
        session.close();
    }

    @Override
    public Room getOneById(Long aLong) {
        return null;
    }

    @Override
    public List<Room> getAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Room ");
        List rooms = query.list();
        session.close();
        return rooms;
    }
}
