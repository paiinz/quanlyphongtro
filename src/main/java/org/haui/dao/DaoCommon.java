package org.haui.dao;

import java.util.List;

public interface DaoCommon<T, K> {
    T create(T t);
    T update(T t);
    void delete(T t);
    void deleteById(K k);
    T getOneById(K k);
    List<T> getAll();

}
