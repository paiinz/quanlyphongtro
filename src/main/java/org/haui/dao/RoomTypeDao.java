package org.haui.dao;

import org.haui.common.util.HibernateUtil;
import org.haui.model.Room;
import org.haui.model.RoomType;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class RoomTypeDao implements DaoCommon<RoomType, Integer> {

    @Override
    public RoomType create(RoomType roomType) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Integer id = (Integer) session.save(roomType);
        transaction.commit();
        session.close();
        roomType.setId(id);
        return roomType;
    }

    @Override
    public RoomType update(RoomType roomType) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(roomType);
        transaction.commit();
        session.close();
        return roomType;
    }

    @Override
    public void delete(RoomType roomType) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(roomType);
        transaction.commit();
        session.close();
    }

    @Override
    public void deleteById(Integer integer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        RoomType roomType = session.createQuery("select RoomType from RoomType where RoomType.id =: id", RoomType.class).setParameter("id", integer).getSingleResult();
        session.delete(roomType);
        transaction.commit();
        session.close();
    }

    @Override
    public RoomType getOneById(Integer integer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        RoomType roomType =  session.createQuery("select RoomType from RoomType where RoomType.id =: id", RoomType.class).setParameter("id", integer).getSingleResult();
        session.close();
        return roomType;
    }

    @Override
    public List<RoomType> getAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from RoomType ");
        List roomTypes = query.list();
        session.close();
        return roomTypes;
    }
}
