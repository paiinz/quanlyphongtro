package org.haui.model;

public enum Role {
    ADMINISTRATOR, NORMAL_USER;
}
