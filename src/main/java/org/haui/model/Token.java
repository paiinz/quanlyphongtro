package org.haui.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "token")
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "token_id")
    private long id;

    private String token;
    private Date expTime;
    private Date createTime;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;
}
