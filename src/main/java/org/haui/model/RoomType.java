package org.haui.model;

import com.sun.xml.bind.v2.model.core.ID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "room_type")
@AllArgsConstructor
@NoArgsConstructor
public class RoomType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_type_id")
    private Integer id;
    private String roomTypeName;
    private int personInRoom;
    private String roomTypeDescription;

    @OneToMany(mappedBy = "roomType")
    private List<Room> rooms;

    @Override
    public String toString() {
        return "Loại phòng " +
                "id=" + id +
                ", Tên ='" + roomTypeName + '\'' +
                ", Tối đa người =" + personInRoom +
                '}';
    }
}
