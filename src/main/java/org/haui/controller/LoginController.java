package org.haui.controller;

import org.apache.commons.lang3.StringUtils;
import org.haui.common.util.SwingUtils;
import org.haui.common.util.UserUtil;
import org.haui.dto.LoginDto;
import org.haui.error.UserError;
import org.haui.model.Token;
import org.haui.model.User;
import org.haui.view.LoginView;
import org.haui.view.MainView;
import org.haui.view.RegisterView;

public class LoginController implements Controller {
    private final LoginView loginView;

    public LoginController(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void initView() {
        this.loginView.setVisible(true);
    }

    @Override
    public void initController() {
        this.initView();
        this.loginView.getBtnRegister().addActionListener(e -> showRegisterForm());
        this.loginView.getBtnLogin().addActionListener(e -> login());
    }

    @Override
    public void exit() {
        this.loginView.setVisible(false);
    }

    private void login() {
        String userName = this.loginView.getTxtUserName().getText();
        String password = this.loginView.getTxtPassword().getText();
        LoginDto loginDto = new LoginDto(userName, password);
        try {
            validateLoginRequest(loginDto);
            Token token = UserUtil.login(loginDto);
            this.onLoginSuccess(token);
        } catch (UserError userError) {
            SwingUtils.showErrorMessage(userError.getMessage(), "Error", this.loginView);
        }
    }

    private void showRegisterForm() {
        RegisterView registerView = new RegisterView();
        RegisterController registerController = new RegisterController(new User(), registerView);
        registerController.initController();
    }

    private void validateLoginRequest(LoginDto loginDto) throws UserError {
        if (StringUtils.isAnyBlank(loginDto.getUserName(), loginDto.getPassword())) {
            throw new UserError("Your username or password is empty.");
        }
    }

    private void onLoginSuccess(Token token) {
        this.loginView.setVisible(false);
        MainController mainController = new MainController(token, new MainView());
        mainController.initController();
    }
}
