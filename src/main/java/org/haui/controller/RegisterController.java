package org.haui.controller;

import org.apache.commons.lang3.StringUtils;
import org.haui.common.util.HibernateUtil;
import org.haui.common.util.SwingUtils;
import org.haui.common.util.UserUtil;
import org.haui.dto.RegisterDto;
import org.haui.error.UserError;
import org.haui.model.User;
import org.haui.view.RegisterView;
import org.hibernate.query.Query;

import javax.swing.*;

public class RegisterController implements Controller{
    private User user;
    private RegisterView registerView;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public RegisterView getRegisterView() {
        return registerView;
    }

    public void setRegisterView(RegisterView registerView) {
        this.registerView = registerView;
    }

    public RegisterController(User user, RegisterView registerView) {
        this.user = user;
        this.registerView = registerView;
    }

    @Override
    public void initView() {
        this.getRegisterView().setVisible(true);
    }

    @Override
    public void initController() {
        this.initView();
        this.getRegisterView().getBtnCancel().addActionListener(e -> exit());
        this.getRegisterView().getBtnRegister().addActionListener(e -> register());
    }

    @Override
    public void exit() {
        this.getRegisterView().setVisible(false);
    }

    private void register() {
        String userName = this.getRegisterView().getTxtUserName().getText();
        System.out.println(userName);
        String password = this.getRegisterView().getTxtPassword().getText();
        String repeatPassword = this.getRegisterView().getTxtRepeatPassword().getText();
        String adminToken = this.getRegisterView().getTxtAdminToken().getText();
        RegisterDto registerDto = new RegisterDto();
        registerDto.setUserName(userName);
        registerDto.setPassword(password);
        registerDto.setRepeatPassword(repeatPassword);
        registerDto.setAdminToken(adminToken);
        try {
            validateRegisterInformation(registerDto);
            if (UserUtil.insertUser(UserUtil.registerDtoToUser(registerDto)) != 0L) {
                SwingUtils.showInformationMessage("User register successfully", "Success", this.getRegisterView());
                this.registerView.setVisible(false);
            } else{
                SwingUtils.showErrorMessage("User register not success!", "Error", this.getRegisterView());
            }
        } catch (UserError error) {
            SwingUtils.showErrorMessage(error.getMessage(), "User error", this.getRegisterView());
        }

    }

    private void validateRegisterInformation(RegisterDto registerDto) throws UserError {
        if (StringUtils.isAnyEmpty(registerDto.getUserName(), registerDto.getPassword(), registerDto.getRepeatPassword())) {
            throw new UserError("Missing information, check your username, password and password confirmation"); //Todo: Create constant or resource bundle
        }

        if (!StringUtils.equals(registerDto.getPassword(), registerDto.getRepeatPassword())) {
            throw new UserError("Your password and password confirmation is not equals."); //Todo: Create constant or resource bundle
        }

        if (UserUtil.isExistUserName(registerDto.getUserName())) {
            throw new UserError("userName is existed in database!");//Todo
        }

        if (StringUtils.equals("admin", registerDto.getAdminToken())) {
            registerDto.setAdministrator(true);
        }
    }

}
