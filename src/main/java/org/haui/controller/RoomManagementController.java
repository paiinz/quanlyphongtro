package org.haui.controller;

import org.haui.common.util.SwingUtils;
import org.haui.dao.RoomDao;
import org.haui.dao.RoomTypeDao;
import org.haui.model.Room;
import org.haui.model.RoomType;
import org.haui.model.listmodel.ComboBoxRoomType;
import org.haui.model.listmodel.ListRoomModel;
import org.haui.model.listmodel.ListRoomTypeModel;
import org.haui.view.RoomManagementView;

import java.util.List;

public class RoomManagementController implements Controller {

    private RoomManagementView roomManagementView;
    private RoomTypeDao roomTypeDao;
    private RoomDao roomDao;
    private RoomType roomTypeTemp = new RoomType(0,"",0,"",null);
    private Room roomTemp = new Room();
    public RoomManagementController(RoomManagementView roomManagementView, RoomTypeDao roomTypeDao, RoomDao roomDao) {
        this.roomManagementView = roomManagementView;
        this.roomTypeDao = roomTypeDao;
        this.roomDao = roomDao;
    }

    public RoomManagementView getRoomManagementView() {
        return roomManagementView;
    }

    public void setRoomManagementView(RoomManagementView roomManagementView) {
        this.roomManagementView = roomManagementView;
    }

    public RoomTypeDao getRoomTypeDao() {
        return roomTypeDao;
    }

    public void setRoomTypeDao(RoomTypeDao roomTypeDao) {
        this.roomTypeDao = roomTypeDao;
    }

    @Override
    public void initView() {
        this.toggleRoomTypeBtnFunction(true);
        this.toggleRoomBtnFunction(true);
        this.refreshListRoomType();
        this.refreshListRoom();
        this.roomManagementView.setVisible(true);
    }

    @Override
    public void initController() {
        this.initView();

        //Room type
        this.getRoomManagementView().getBtnAddRoomType().addActionListener(l -> insertRoomType());
        this.getRoomManagementView().getListRoomType().addListSelectionListener(l -> onChooseRoomType());
        this.getRoomManagementView().getBtnCancelRoomType().addActionListener(l -> onClickBtnCancelRoomType());
        this.getRoomManagementView().getBtnUpdateRoomType().addActionListener(l -> onClickBtnUpdateRoomType());
        this.getRoomManagementView().getBtnDeleteRoomType().addActionListener(l -> onClickBtnDeleteRoomType());

        //Room
        this.getRoomManagementView().getBtnAddRoom().addActionListener(l -> insertRoom());
        this.getRoomManagementView().getListRoom().addListSelectionListener(l -> onChooseRoom());
        this.getRoomManagementView().getBtnCancelRoom().addActionListener(l -> onClickBtnCancelRoom());
        this.getRoomManagementView().getBtnDeleteRoom().addActionListener(l -> onClickBtnDeleteRoom());
        this.getRoomManagementView().getBtnUpdateRoom().addActionListener(l -> onClickBtnUpdateRoom());
    }


    @Override
    public void exit() {

    }

    //Functions for room type
    private void refreshListRoomType() {
        ListRoomTypeModel listRoomTypeModel = new ListRoomTypeModel();
        ComboBoxRoomType comboBoxRoomType = new ComboBoxRoomType();
        List list = this.roomTypeDao.getAll();
        listRoomTypeModel.addAll(list);
        comboBoxRoomType.addAll(list);
        this.getRoomManagementView().getListRoomType().setModel(listRoomTypeModel);
        this.getRoomManagementView().getCbRoomType().setModel(comboBoxRoomType);
    }

    private void onClickBtnUpdateRoomType() {
        String roomTypeName = this.getRoomManagementView().getTxtRoomTypeName().getText();
        String roomTypeDes = this.getRoomManagementView().getTxtRoomTypeDes().getText();
        int roomTypePerson = Integer.parseInt(this.getRoomManagementView().getTxtRoomTypePerson().getText());

        roomTypeTemp.setPersonInRoom(roomTypePerson);
        roomTypeTemp.setRoomTypeDescription(roomTypeDes);
        roomTypeTemp.setRoomTypeName(roomTypeName);

        this.roomTypeDao.update(roomTypeTemp);
        SwingUtils.showInformationMessage("Updated!", "Updated", this.getRoomManagementView());
        this.refreshListRoomType();
    }

    private void onClickBtnCancelRoomType() {
        this.toggleRoomTypeBtnFunction(true);
    }

    private void insertRoomType() {
        String roomTypeName = this.getRoomManagementView().getTxtRoomTypeName().getText();
        String roomTypeDes = this.getRoomManagementView().getTxtRoomTypeDes().getText();
        int roomTypePerson = Integer.parseInt(this.getRoomManagementView().getTxtRoomTypePerson().getText());

        RoomType roomType = new RoomType(null, roomTypeName,roomTypePerson, roomTypeDes, null);
        RoomType created = this.roomTypeDao.create(roomType);
        if (created.getId() > 0) {
            SwingUtils.showInformationMessage("Created : " + created.getId(), "Thêm loại phòng thành công", this.getRoomManagementView());
        } else {
            SwingUtils.showErrorMessage("Failed", "Error", this.getRoomManagementView());
        }
        this.refreshListRoomType();
    }

    private void onChooseRoomType() {
        this.roomTypeTemp = (RoomType) this.getRoomManagementView().getListRoomType().getSelectedValue();
        if (this.roomTypeTemp != null) {
            populateRoomTypeTextBox();
            this.toggleRoomTypeBtnFunction(false);
        }
    }

    private void onClickBtnDeleteRoomType() {
        this.roomTypeDao.delete(roomTypeTemp);
        this.refreshListRoomType();
        this.toggleRoomTypeBtnFunction(true);
    }

    private void populateRoomTypeTextBox() {
        this.getRoomManagementView().getTxtRoomTypeName().setText(this.roomTypeTemp.getRoomTypeName());
        this.getRoomManagementView().getTxtRoomTypeDes().setText(this.roomTypeTemp.getRoomTypeDescription());
        this.getRoomManagementView().getTxtRoomTypePerson().setText(String.valueOf(this.roomTypeTemp.getPersonInRoom()));
    }

    private void toggleRoomTypeBtnFunction(boolean f) {
        if (f) {
            roomTypeTemp = new RoomType(0,"",0,"",null);
        }
        this.getRoomManagementView().getBtnAddRoomType().setEnabled(f);
        this.getRoomManagementView().getBtnUpdateRoomType().setEnabled(!f);
        this.getRoomManagementView().getBtnDeleteRoomType().setEnabled(!f);
        this.getRoomManagementView().getBtnCancelRoomType().setEnabled(!f);
    }
    //End functions for roomtype



    //Functions for room
    private void onChooseRoom() {
        this.roomTemp = (Room) this.getRoomManagementView().getListRoom().getSelectedValue();
        if (this.roomTemp != null) {
            populateRoomTextBox();
            this.toggleRoomBtnFunction(false);
        }
    }

    private void refreshListRoom() {
        ListRoomModel listRoomModel = new ListRoomModel();
        listRoomModel.addAll(this.roomDao.getAll());
        this.getRoomManagementView().getListRoom().setModel(listRoomModel);
    }

    private void onClickBtnDeleteRoom() {
        this.roomDao.delete(roomTemp);
        this.refreshListRoom();
        this.toggleRoomBtnFunction(true);
    }

    private void onClickBtnCancelRoom() {
        this.toggleRoomBtnFunction(true);
    }

    private void insertRoom() {
        String roomName = this.roomManagementView.getTxtRoomName().getText();
        double roomPrice = Double.parseDouble(this.roomManagementView.getTxtRoomPrice().getText());
        String roomAddress = this.roomManagementView.getTxtRoomAddress().getText();
        RoomType roomType = (RoomType) this.roomManagementView.getCbRoomType().getSelectedItem();
        Room room = new Room(null, roomName, roomType, roomPrice, true, roomAddress, null);
        Room created = this.roomDao.create(room);
        if (created.getId() > 0) {
            SwingUtils.showInformationMessage("Created : " + created.getId(), "Thêm loại phòng thành công", this.getRoomManagementView());
        } else {
            SwingUtils.showErrorMessage("Failed", "Error", this.getRoomManagementView());
        }
        this.refreshListRoom();
    }

    private void onClickBtnUpdateRoom() {
        String roomName = this.roomManagementView.getTxtRoomName().getText();
        double roomPrice = Double.parseDouble(this.roomManagementView.getTxtRoomPrice().getText());
        String roomAddress = this.roomManagementView.getTxtRoomAddress().getText();
        RoomType roomType = (RoomType) this.roomManagementView.getCbRoomType().getSelectedItem();

        this.roomTemp.setAddress(roomAddress);
        this.roomTemp.setAvailable(true);
        this.roomTemp.setRoomName(roomName);
        this.roomTemp.setRoomPrice(roomPrice);
        this.roomTemp.setRoomType(roomType);

        this.roomDao.update(roomTemp);

        this.refreshListRoom();
    }

    private void populateRoomTextBox() {
        this.roomManagementView.getTxtRoomName().setText(this.roomTemp.getRoomName());
        this.roomManagementView.getTxtRoomPrice().setText(String.valueOf(this.roomTemp.getRoomPrice()));
        this.roomManagementView.getTxtRoomAddress().setText(this.roomTemp.getAddress());
        RoomType roomType = null;
        for(int i=0 ; i < this.roomManagementView.getCbRoomType().getModel().getSize(); i++) {
            roomType = (RoomType) this.roomManagementView.getCbRoomType().getModel().getElementAt(i);
            if (roomType.getId().equals(this.roomTemp.getRoomType().getId())) {
                this.roomManagementView.getCbRoomType().setSelectedIndex(i);
                break;
            }
        }

    }

    private void toggleRoomBtnFunction(boolean f) {
        if (f) {
            roomTemp = new Room();
        }
        this.getRoomManagementView().getBtnAddRoom().setEnabled(f);
        this.getRoomManagementView().getBtnUpdateRoom().setEnabled(!f);
        this.getRoomManagementView().getBtnDeleteRoom().setEnabled(!f);
        this.getRoomManagementView().getBtnCancelRoom().setEnabled(!f);
    }

    //End functions for room




}
