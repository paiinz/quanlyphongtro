package org.haui.controller;

import org.apache.commons.lang3.StringUtils;
import org.haui.dao.RoomDao;
import org.haui.dao.RoomTypeDao;
import org.haui.model.Token;
import org.haui.model.User;
import org.haui.view.MainView;
import org.haui.view.RoomManagementView;

public class MainController implements Controller {

    private Token token;
    private MainView mainView;
    private User user;

    public MainController(Token token, MainView mainView) {
        this.token = token;
        this.mainView = mainView;
        this.user = token.getUser();
    }

    @Override
    public void initView() {
        this.mainView.getLblWelcomeBack().setText(StringUtils.replace(this.mainView.getLblWelcomeBack().getText(), "{{userName}}", this.user.getUserName()));
        this.mainView.setVisible(true);
    }

    @Override
    public void initController() {
        initView();
        this.mainView.getBtnRoomManagement().addActionListener(l -> showRoomManagement());
    }

    @Override
    public void exit() {

    }

    private void showRoomManagement() {
        RoomManagementController roomManagementController = new RoomManagementController(new RoomManagementView(), new RoomTypeDao(), new RoomDao());
        roomManagementController.initController();
    }
}
