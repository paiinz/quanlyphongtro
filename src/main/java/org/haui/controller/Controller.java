package org.haui.controller;

public interface Controller {
    void initView();
    void initController();
    void exit();
}
