package org.haui;

import org.haui.common.util.HibernateUtil;
import org.haui.controller.LoginController;
import org.haui.view.LoginView;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        LoginView loginView = new LoginView();
        LoginController loginController = new LoginController(loginView);
        loginController.initController();
    }
}
