package org.haui.common.util;

import org.haui.common.constants.SqlParamaters;
import org.haui.common.constants.SqlQueries;
import org.haui.common.constants.UserConstants;
import org.haui.dto.LoginDto;
import org.haui.dto.RegisterDto;
import org.haui.error.UserError;
import org.haui.model.Role;
import org.haui.model.Token;
import org.haui.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;

public class UserUtil {
    public static boolean isExistUserName(String userName) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(SqlQueries.GET_USER_NAME_BY_USER_NAME);
        query.setParameter(SqlParamaters.USER_NAME, userName);
        boolean result = query.getResultList().isEmpty();
        session.close();
        return !result;
    }

    public static Long insertUser(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Long userSavedId = (Long) session.save(user);
        session.close();
        return userSavedId;
    }

    public static Token login(LoginDto loginDto) throws UserError {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<User> query = session.createQuery(SqlQueries.GET_USER_BY_USER_NAME_AND_PASSWORD, User.class);
        query.setParameter(SqlParamaters.USER_NAME, loginDto.getUserName());
        query.setParameter(SqlParamaters.PASSWORD, loginDto.getPassword());
        User user = query.uniqueResult();
        if (user == null) {
            throw new UserError("Your userName and password are mismatch!");
        }

        session.close();
        return createToken(user);
    }

    public static User registerDtoToUser(RegisterDto registerDto) {
        User user = new User();
        user.setUserName(registerDto.getUserName());
        user.setPassword(registerDto.getPassword());
        if (registerDto.isAdministrator()) {
            user.setRole(UserConstants.ROLE_ADMIN);
        } else {
            user.setRole(UserConstants.ROLE_USER);
        }
        return user;
    }

    public static void validateToken(Token token) throws UserError {
        if (new Date().after(token.getExpTime())) {
            throw new UserError("Token is expired! Please login and try again."); //Todo
        }

    }

    public static int deleteAllExistToken(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        for (Token token: user.getTokens()) {
            session.delete(token);
        }
        transaction.commit();
        session.close();
        return 1;
    }

    public static Token createToken(User user) {
        deleteAllExistToken(user);
        Token token = new Token();
        Date now = new Date();
        String tokenPlainText = user.getUserName() + now.toInstant().getEpochSecond();//Todo
        token.setToken(tokenPlainText);
        token.setCreateTime(now);
        token.setExpTime(DateUtil.addDays(now, 1));
        token.setUser(user);
        Session session = HibernateUtil.getSessionFactory().openSession();
        token.setId((Long) session.save(token));
        session.close();
        return token;
    }
}
