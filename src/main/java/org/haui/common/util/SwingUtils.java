package org.haui.common.util;

import javax.swing.*;
import java.awt.*;

public class SwingUtils {
    public static void showErrorMessage(String message,String title, Frame frame) {
        JOptionPane.showMessageDialog(frame, message, title, JOptionPane.ERROR_MESSAGE);
    }
    public static void showInformationMessage(String message,String title, Frame frame) {
        JOptionPane.showMessageDialog(frame, message, title, JOptionPane.INFORMATION_MESSAGE);
    }
}
