package org.haui.common.constants;

public class SqlQueries {
    public static final String GET_USER_NAME_BY_USER_NAME = "SELECT " + SqlParamaters.USER_NAME + " FROM " + SqlParamaters.USER_HBL + " where " + SqlParamaters.USER_NAME + "=:" + SqlParamaters.USER_NAME;
    public static final String GET_USER_BY_USER_NAME_AND_PASSWORD = "FROM " + SqlParamaters.USER_HBL + " u where " + SqlParamaters.USER_NAME + " =: " + SqlParamaters.USER_NAME + " AND " + SqlParamaters.PASSWORD + " =: " + SqlParamaters.PASSWORD;
}
