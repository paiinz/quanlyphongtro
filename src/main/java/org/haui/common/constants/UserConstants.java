package org.haui.common.constants;

public class UserConstants {
    public static final String ROLE_ADMIN = "Administrator";
    public static final String ROLE_USER = "User";

}
